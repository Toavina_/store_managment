## Store managment made with Django

### Dependencies

- Django 2.2
- Mysql and mysql-client python

### Get started

Clone repository

    git clone git@gitlab.com:Toavina_/store_managment.git

Run migrations

    python manage.py migrate

Run server
    
    python manage.py runserver

### Contribution

Push your contribution in your own branch ```user_name/feature(app)``` 
